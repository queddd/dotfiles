export PS1='\[\033[01;31m\]\h\[\033[01;34m\] \w \$\[\033[00m\] '
#export PS1="[\e[35m\e[1m\w\e[0m](\e[36m\u\e[0m)\e[31m$\e[0m "
#export PS1="(\D{%m-%y-%Y} \T)<\u@\h|\w>$ "
export PATH="$PATH:$HOME/config:$HOME/.local/bin"
export TERM=xterm
export SUDO="sudo"
export EDITOR=vim # Change to editor of yoru choice
set -o emacs 

if [ $(uname -s) == "OpenBSD" ] 
then
	alias install="$SUDO pkg_add"
	alias remove="$SUDO pkg_delete"
	alias search="$SUDO pkg_info -Q"
else
	source /etc/os-release
	if [ $ID == "debian" ]
	then
		alias install="$SUDO apt-get install -y"
		alias upgrade="$SUDO apt upgrade"
		alias remove="$SUDO apt purge"
		alias update="$SUDO apt update"
		alias search="$SUDO apt search"
	fi
	if [ $ID == "void" ]
	then
		alias install="$SUDO xbps-install -y"
		alias upgrade="$SUDO xbps-install -Syu"
		alias remove="$SUDO xbps-remove -y"
		alias update="$SUDO xbps-install -S"
		alias search="$SUDO xbps-query -Rs"
	fi
	if [ $ID == "alpine" ]
	then
		alias install="$SUDO apk add"
		alias remove="$SUDO apk del"
		alias update="$SUDO apk update"
		alias search="$SUDO apk search"
	fi
fi


if ! command -v clear &> /dev/null
then
        alias cl="printf '\033[2J'"
	alias cls="printf '\033[2J'"
        alias clear="printf '\033[2J'"

else
	alias cls="clear"
	alias cl="clear"
fi

psl(){
  ps aux | grep "$@"
}
alias l="ls"
alias ll="ls -lah"
alias ro="$SUDO reboot" 
alias lo="logout"
alias su="$SUDO -s"
alias ka="$SUDO killall"
alias df="df -h"
alias cp="cp -v"
alias ..="cd .."
alias ...="cd ../../"
alias bashrc="$EDITOR ~/.bashrc && source ~/.bashrc"
alias feh="feh -."
alias mount="$SUDO mount" 
alias umount="$SUDO umount" 
#alias ls="ls -pClah --color=auto --group-directories-first -F"
alias ls="ls -pC --color=auto --group-directories-first -F"
alias openvpn="$SUDO openvpn" 
