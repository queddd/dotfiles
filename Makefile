install:
	mkdir -p ~/.config/foot ~/.config/i3 ~/.config/wmderland ~/.icewm ~/config/
	cp icewm_keys.conf ~/.icewm/keys
	cp scripts/* ~/config 
	cp tmux.conf ~/.tmux.conf
#	cp bashrc ~/.bashrc
	cp Xresources ~/.Xresources
	cp i3.conf ~/.config/i3/config
	cp wmderland.conf ~/.config/wmderland/config
	cp etc/X11/* /etc/X11/
	
